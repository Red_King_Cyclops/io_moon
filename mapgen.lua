--minetest.clear_registered_ores()
minetest.clear_registered_biomes()
minetest.clear_registered_decorations()

--normal io biome
	minetest.register_biome({
		name = "io_moon",
		--node_dust = "default:gravel",
		node_top = "io_moon:ground_rocks",
		depth_top = 1,
		node_filler = "io_moon:stone",
		depth_filler = 3,
		node_stone = "io_moon:stone",
		node_water_top = "default:lava_source",
		depth_water_top = 1 ,
		node_water = "default:lava_source",
		node_river_water = "default:lava_source",
		y_min = -31000,
		y_max = 200,
		heat_point = 100,
		humidity_point = 0,
	})

--lava ore
	minetest.register_ore({
		ore_type       = "scatter",
		ore            = "default:lava_source",
		wherein        = "io_moon:stone",
		clust_scarcity = 9 * 9 * 9,
		clust_num_ores = 12,
		clust_size     = 3,
		y_min          = -31000,
		y_max          = 31000,
	})

	minetest.register_ore({
		ore_type       = "scatter",
		ore            = "default:lava_source",
		wherein        = "io_moon:ground_rocks",
		clust_scarcity = 12 * 12 * 12,
		clust_num_ores = 15,
		clust_size     = 5,
		y_min          = -31000,
		y_max          = 31000,
	})
