# io_moon

This mod replaces the default terrain with Io, a volcanic moon of Jupiter. Io is more volcanic than Earth. Lava is everywhere, but it is more common underground. Mountains, or should I say volcanos, are coated with lava. There are lava lakes, too.

At the moment there is no oxygen system and no way to survive on Io without using creative mode. Dokimi's [molten_sailor](https://forum.minetest.net/viewtopic.php?f=9&t=22494) mod is recommended.

The code is licenced under LGPL 2.1 and the media, except for the Jupiter image, are licenced under CC-BY-SA 3.0. The Jupiter image is taken from https://www.flickr.com/photos/nasahubble/46616136772 , which is licenced under CC-BY-2.0. The io_moon_stone.png and io_moon_ground_rocks.png textures are modified versions of images from default.