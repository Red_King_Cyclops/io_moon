minetest.register_on_joinplayer(function(player)
	minetest.after(0, function()
		textures ={
		"dark_sky.png",
		"dark_sky.png",
		"dark_sky.png",
		"sky_neg_z.png",	
		"dark_sky.png",
		"dark_sky.png",
		}
		
		player:set_sky({r=0, g=0, b=0, a=0},"skybox", textures)
		player:set_clouds({density = 0})
		
		player:set_physics_override(1, 0.6, 0.2) -- speed, jump, gravity
	end)
end)
