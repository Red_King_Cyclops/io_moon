minetest.register_node("io_moon:stone", {
    description = "Io Stone",
    tiles = {"io_moon_stone.png"},
    is_ground_content = true,
    groups = {cracky=3},
    sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("io_moon:ground_rocks", {
    description = "Io Ground Rocks",
    tiles = {"io_moon_ground_rocks.png"},
    is_ground_content = true,
    groups = {crumbly=1, falling_node = 1},
    sounds = default.node_sound_gravel_defaults()
})
